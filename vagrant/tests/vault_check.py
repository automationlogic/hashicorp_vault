#!/usr/bin/env python3

import requests
from sys import exit

leader_node = "vault-1"
vault_nodes = 3
timeout = 3

def check_node(node):
    url = f"http://{node}:8200/v1/sys/health"
    try:
        r = requests.get(url, timeout = timeout)
    except requests.exceptions.ConnectionError:
        status = "ERROR"
        info = "Node down"
    else:
        if r.status_code in [200, 429]:
            status = "OK"
            info = "Node up, unsealed"
        elif r.status_code in [501, 503]:
            status = "WARN"
            info = "Node up, sealed"
    return status, info

for i in range(1,vault_nodes + 1):
    node = f"vault-{i}"
    type = node == leader_node and "Leader" or "Follower"
    (status, info) = check_node(node)
    print(f"[{status:^5}] [{type:^8}] [{node:}] [{info}] ")
