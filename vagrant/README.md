# HashiCorp Vault Test Environment

## Getting Started

The package creates few vagrant guest nodes (Ubuntu 18.04) and installs Hashicorp Vault software on them.

Prerequisite packages are installed, Vault user is created, and directory structure for Vault is created.

Vault archive is downloaded from Hashicorp web server, checksum checked, and vault binary extracted to bin directory. Existing binary is not replaced unless {{ replace_software }} variable is set to true.

Several Vault nodes are set up. One node (leader node) is initialized and initialization data (like secret keys, root token id) is saved to secrets directory on host machine.

Follower nodes are set up to join the leader node to form a cluster. All the nodes are unsealed by default.

Vault guests are set with private network interfaces to enable inter node traffic (required for Vault cluster communication).
Vault process is set to listen on 8200/tcp port and cluster traffic is set to 8201/tcp port. TLS is disabled.

### Prerequisites

Running the test environment requires the following software:
* Linux or MacOS box, tested on Ubuntu Linux 18.04 LTS
* Virtual Box, tested on v6.0.20
* Vagrant , tested on v2.2.7, version is enforced in Vagrantfile
  * All plugins are installed when vagrant runs for the first time
* Ansible, tested on v2.9.7

Running test script requires:
* python 3, tested on v3.6
* python requests package, tested on v2.18.4

### Installing

Install prerequisite packages, and clone the code repository.

## Running the tests

There is a vault_check.py python script in tests/ directory.
It checks whether Vault nodes are up and if the vault nodes are sealed or unsealed .


## Deployment

Run `vagrant up` from main directory (where Vagrantfile is located).

Secret keys and root token can be found on host in code/secrets directory.

Vault cluster should form and all the nodes will be unsealed. If you need to unseal the nodes manually, use the saved secret keys.

Login to leader node (use root token to login) and check if all nodes joined the cluster (Status -> Raft Storage)

## Known Bugs

## Authors

* **Bartlomiej Bezulski** - *Initial work*
