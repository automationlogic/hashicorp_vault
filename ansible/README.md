# HashiCorp Vault Test Environment

## Getting Started

### Prerequisites

* Ansible - tested on v2.9.7 and v2.9.11
* Infrastructure, either local using vagrant or on aws using terraform. See READMEs in those directories for detailed instructions.

## Deployment

### Vagrant

If running as part of a vagrant testing ansible will run automatically using the vagrant provisioner.

### Terraform

Follow the instructions in the terraform README to build the required infrastructure. In order to run ansible locally you will need an ssh config.

`~/.ssh/config`
```
Host <bastion-ip>
  User ubuntu
  IdentityFile ~/.ssh/deploy.pem

Host 10.0.*
  User ubuntu
  IdentityFile ~/.ssh/deploy.pem
  ProxyJump <bastion-ip>
```

Replace `<bastion-ip>` with the public IP of the bastion server created by terraform. Note the the VPC has a very large address space which may conflict with other entries in your ssh config.

A dynamic inventory has been configured with the dynamic inventory plugin for aws ec2.

Run `ansible-playbook -i inventory/test.aws_ec2.yml playbook.yml` to install and configure vault

Secret keys and root token can be found on host in code/secrets directory.

Vault cluster should form and all the nodes will be unsealed. If you need to unseal the nodes manually, use the saved secret keys.

Login to leader node (use root token to login) and check if all nodes joined the cluster (Status -> Raft Storage)

## Known Bugs

## Authors

* **Bartlomiej Bezulski** - *Initial work*

* **Anna Hackett Boyle**
