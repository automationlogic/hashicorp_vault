region  = "eu-central-1"

tfstate_s3_bucket_name = "automationlogic-terraform-hashicorp-vault-state"
dynamodb_lock_table = "terraform-hashicorp-vault-locks"
