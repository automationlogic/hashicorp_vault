variable "region" {
  type        = string
  description = "aws region to place resources into"
}

variable "tfstate_s3_bucket_name" {
  type        = string
  description = "s3 bucket to keep the terraform state file in"
}

variable "dynamodb_lock_table" {
  type        = string
  description = "dynamodb to use for terraform locking"
}

variable "key_name" {
  type        = string
  description = "name of the deployment key"
  default     = "deploy"
}

variable "public_key" {
  type        = string
  description = "public key of ssh keypair used to access VMs. Either generate a new key or use an existing key, and paste the public key here"
}
