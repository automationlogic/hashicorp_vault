output "s3_bucket" {
  value       = aws_s3_bucket.terraform_state.arn
  description = "the arn of the s3 bucket"
}

output "dynamodb_table" {
  value       = aws_dynamodb_table.terraform_locks.name
  description = "the name of the dynamodb table"
}
 ##TO-DO add key details here and then use remote state data source to access later
 ## Need to figure out bootstrapping issues first tho
