provider "aws" {
  region = var.region
}

terraform {
  required_version = ">= 0.12.0"
  # backend "s3" {
  #   key    = "backend/terraform.tfstate"
  # }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = var.tfstate_s3_bucket_name

  lifecycle {
    prevent_destroy = true
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = var.dynamodb_lock_table
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = var.key_name
  public_key = var.public_key
}
