terraform {
  required_version = ">= 0.12.0"
}

data aws_ami "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = [var.image_name]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

data "aws_availability_zones" "availability_zones" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

resource "aws_instance" "node" {
  count                  = 3
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type

  subnet_id              = var.subnet_ids[count.index]
  vpc_security_group_ids = [ aws_security_group.vault_server_sg.id ]

  key_name               = "deploy"

  tags = {
    Name         = "vault-${(count.index + 1)}"
    Project      = "hashicorp vault"
    Environment  = "test"
    Vault_Leader = "${count.index == 0 ? "True" : "False"}"
  }
}

resource "aws_security_group" "vault_server_sg" {
  name        = "bastion_access"
  vpc_id      = var.vault_vpc_id

  tags = {
    Name        = "Vault Server Security Group"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_security_group_rule" "bastion_access" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = var.bastion_security_group_id
  security_group_id        = aws_security_group.vault_server_sg.id
}

#TODO - this is probably too permissive for a real life client, consider restricting
resource "aws_security_group_rule" "outbound_access" {
  type                     = "egress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.vault_server_sg.id
}

resource "aws_security_group_rule" "client_access_to_vault" {
  type                     = "ingress"
  from_port                = 8200
  to_port                  = 8200
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.vault_server_sg.id
  security_group_id        = aws_security_group.vault_server_sg.id
}

resource "aws_security_group_rule" "vault_cluster_comms" {
  type                     = "ingress"
  from_port                = 8201
  to_port                  = 8201
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.vault_server_sg.id
  security_group_id        = aws_security_group.vault_server_sg.id
}
