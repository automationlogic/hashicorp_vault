variable "image_name" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "vault_vpc_id" {
   type = string
 }

 variable "subnet_ids" {
   type = list
 }

 variable "bastion_security_group_id" {
   type = string
 }
