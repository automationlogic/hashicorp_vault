# HashiCorp Vault Test Environment

## Getting Started

TBD

### Prerequisites

Running the code requires terraform software, it can be obtained from:
https://www.terraform.io/downloads.html

Code was tested on Terraform v0.12.23.

### Installing

Download the terraform binary, unpack it and set the PATH environment variable if necessary.

## Running the tests

TBD

## Deployment

### Backend

Before running the demo you need to set up your AWS environment. This is the purpose of the backend directory. It sets up the basic prerequisites
for running a terraform deployment in the cloud: s3 bucket for remote state, locking mechanism and shared deployment key.
The variable `public_key` has been left undefined in the intention that you will supply it when running for the first time.

Initially the S3 bucket and DynamoDB need to be created and then they can be used. Terraform code to run can be found under `backend` directory.

Make sure that the following code is commented out in `main.tf` file:

```
# terraform {
#   backend "s3" {
#     key    = backend/terraform.tfstate"
#   }
# }
```

You need to set up names of S3 bucket (need to be unique globally unfortunately), dynamodb table and region. Due to limitations of terraform backends, you need to set the information in `backend.hcl` as well as in the usual `terraform.tfvars`.

Then you need to run `terraform init` and then `terraform apply` toset up S3 bucket and dynamodb.

Next you need to remove the comments mentioned above, and you can initialize remote backend by running `terraform init -backend-config=backend.hcl`.

It is not recommended to show these steps as part of a client demo as these are basic pre-reqs that any client would need to deploy terraform.

### VPC

Once the backend has run, we can create the VPC and associated network components.

In the `test/vpc` directory:

run `terraform init -backend-config=backend.hcl` then `terraform apply` to build this.

`backend.hcl` should use the same s3 bucket that you created in the backend section.

Whether or not this is useful to a client depends quite strongly on whether they want us to integrate vault into their existing cloud infrastructure or
deploy vault on a completely stand alone basis. If the latter, we should probably consider reducing the size of the VPC address space as it is far
larger than needed.

### Services

Once the networking infrastructure has been built we can run terraform in the `test/services` directory to build the vault infrastructure and the bastion.

*If you intend to run ansible locally after this you will need to add your IP address to the* `ssh_to_bastion` *security group rule.*

Run `terraform init -backend-config=backend.hcl` then `terraform apply` to build this.

`backend.hcl` should use the same s3 bucket that you created in the backend section.

Instance type is t2.micro purely for test purposes, for a real demo we should use the VM sizes in the reference architecture:https://learn.hashicorp.com/vault/operations/raft-reference-architecture#recommended-architecture - related, we should use more variables rather than hardcoding.

There's not much to say about this other than this probably isn't the most sensible place for the bastion code to live, it should maybe be in the vpc section
or its own module.

Currently we use aws data sources to get information on the networking infra built in the previous step, but potentially it is more secure to use the remote state data source.

If you are spinning infrastructure up and down a lot as part of testing it might make more sense to create an elastic IP as part of the VPC build and assign it to the bastion to avoid having to constantly modify your ssh config.
*OR* do something really clever and work out a way to auto generate an ssh config each time terraform runs...

### Modules

The build of the vault cluster is encapsulated in a module in order that we can provide a generic solution to clients. It spins up three vault servers, associated security groups and rules. In future, we could potentially consider releasing this in a stand alone form under an apache licence.

It's important to note that the 'name' tag must be set to the value it currently is for ansible to work properly.

To-dos:
- Make module more generic through use of more variables
- Number of vault nodes, currently we use three but the best practice is to use 5 across 3 availability zones: https://learn.hashicorp.com/vault/operations/raft-reference-architecture#recommended-architecture. This should probably be configurable by some kind of variable as per point one.

Currently the bastion build is not modularised under the assumption that most clients will have some form of bastion/access set-up already. There might
be value in doing this later on, especially if we are proposing a completely isolated architecture for vault. We would probably need to consider hardening
the bastion if this was the case.

## Known Bugs

## Authors

* **Bartlomiej Bezulski** - *Initial work*

* **Anna Hackett Boyle**
