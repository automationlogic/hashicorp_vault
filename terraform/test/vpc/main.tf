provider "aws" {
  region = var.region
}

terraform {
  required_version = ">= 0.12.0"
  backend "s3" {
    key            = "test/vpc/terraform.tfstate"
  }
}

data "aws_availability_zones" "availability_zones" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

resource "aws_vpc" "vault_test-vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name        = "vpc"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_subnet" "private-sn" {
  count = 3
  vpc_id            = aws_vpc.vault_test-vpc.id
  availability_zone = data.aws_availability_zones.availability_zones.names[count.index]

  cidr_block        = "10.0.${count.index + 1}.0/24"


  tags = {
    Name        = "private-subnet-${count.index + 1}"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_subnet" "public-sn" {
  count = 3

  vpc_id            = aws_vpc.vault_test-vpc.id
  availability_zone = data.aws_availability_zones.availability_zones.names[count.index]

  cidr_block        = "10.0.1${count.index + 1}.0/24"


  tags = {
    Name        = "public-subnet-${count.index + 1}"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vault_test-vpc.id

  tags = {
    Name        = "internet-gateway"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_nat_gateway" "natgw" {
  allocation_id = aws_eip.eip-natgw.id
  subnet_id     = aws_subnet.public-sn[0].id

  tags = {
    Name        = "nat-gateway"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_eip" "eip-natgw" {
  vpc = true

  tags = {
    Name        = "elastic-ip nat-gateway"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.vault_test-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name        = "public-route_table"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.vault_test-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgw.id
  }

  tags = {
    Name        = "private-route_table"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_route_table_association" "public-rt" {
  count = 3

  subnet_id      = aws_subnet.public-sn[count.index].id
  route_table_id = aws_route_table.public-rt.id
}

resource "aws_route_table_association" "private-rt" {
  count = 3

  subnet_id      = aws_subnet.private-sn[count.index].id
  route_table_id = aws_route_table.private-rt.id
}
