bucket         = "automationlogic-terraform-hashicorp-vault-state"
region         = "eu-central-1"

dynamodb_table = "terraform-hashicorp-vault-locks"
encrypt        = true
