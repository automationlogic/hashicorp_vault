variable "region" {
  type        = string
  description = "aws region to place resources into"
}

variable "key_name" {
  type = string
}

variable "public_key" {
  type = string
}
