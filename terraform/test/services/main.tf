provider "aws" {
  region = var.region
}

terraform {
  required_version = ">= 0.12.0"
  backend "s3" {
    key            = "test/services/terraform.tfstate"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20200611"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

data "aws_vpc" "vault_vpc" {
  tags = {
    Name        = "vpc"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

data "aws_subnet_ids" "private_subnets" {
  vpc_id = "${data.aws_vpc.vault_vpc.id}"

  tags = {
    Project     = "hashicorp vault"
    Name        = "private-subnet-*"
  }

}

data "aws_subnet_ids" "public_subnets" {
  vpc_id = "${data.aws_vpc.vault_vpc.id}"

  tags = {
    Project     = "hashicorp vault"
    Name        = "public-subnet-*"
  }

}


module "vault_servers" {
  source                    = "../../modules/services/vault_node"
  image_name                = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04*"

  instance_type             = "t2.micro"
  vault_vpc_id              = data.aws_vpc.vault_vpc.id

  subnet_ids                = data.aws_subnet_ids.private_subnets.ids
  bastion_security_group_id = aws_security_group.bastion_security_group.id

}

resource "aws_instance" "bastion" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  subnet_id                   = sort(data.aws_subnet_ids.public_subnets.ids)[0]
  vpc_security_group_ids      = [ aws_security_group.bastion_security_group.id ]
  associate_public_ip_address = true

  key_name                    = "deploy" #TODO some how make this automagically pull from backend set-up

  tags = {
    Name        = "bastion-host"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_security_group" "bastion_security_group" {
  name        = "ssh_access"
  vpc_id      = data.aws_vpc.vault_vpc.id

  tags = {
    Name        = "Bastion Security Group"
    Project     = "hashicorp vault"
    Environment = "test"
  }
}

resource "aws_security_group_rule" "outbound_access" {
  type                     = "egress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.bastion_security_group.id
}

resource "aws_security_group_rule" "ssh_to_bastion" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  cidr_blocks              = ["77.108.144.130/32"]
  security_group_id        = aws_security_group.bastion_security_group.id
}
