output "bastion_ip" {
  value       = aws_instance.bastion.public_ip
  description = "bastion ip address. update your ssh config if you want to run ansible locally"
}
